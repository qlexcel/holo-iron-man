![](/4.Docs/Front.jpg)

# HoloIronMan--钢铁侠摆件

**视频介绍：** https://www.bilibili.com/video/BV1i44y1E7dC/

## 0. 关于本项目
基于holocubic制作的钢铁侠摆件，颜值高又实用的礼物！


资料全部开源，成本150以内，欢迎DIY！

做了两个版本，一个是钢铁侠版本，对应的PCB是`1.Hardware\MainBoard`和`1.Hardware\ExtendBoard`，对应的3D模型是`3.3D Model\IronMan`

![](/4.Docs/IronMan.jpg)

还有一个是Q版钢铁侠版本，对应的PCB是`1.Hardware\Q-MainBoard`和`1.Hardware\Q-ExtendBoard`，对应的3D模型是`3.3D Model\IronMan-Q`

模型配色可以自己随意修改，比如上红黄蓝

![](/4.Docs/RYB.jpg)

上银色

![](/4.Docs/Silvery.jpg)

## 1.操作说明
左右倾斜，切换选择各类APP。

向前倾斜1s钟即可进入当前页的APP应用，后仰1s退出该APP。

## 2.功能说明
### 时间天气显示

![](/4.Docs/tianqi.jpg)

调用如下api获取时间信息：http://api.m.taobao.com/rest/api3.do?api=mtop.common.getTimestamp

调用https://www.tianqiapi.com提供的api接口，每隔一段时间更新天气信息

### 图片显示
![](/4.Docs/longmao.jpg)

显示保存到SD卡中的图片，可以当成电子相框来使用。

### 视频播放
![](/4.Docs/shipin.jpg)

播放保存到SD卡中的视频。

### 特效显示
![](/4.Docs/texiao.jpg)

播放内置的几种特效。

### B站信息
![](/4.Docs/Bili.jpg)

显示自己B站的头像、关注数、粉丝数。

### 2048小游戏
![](/4.Docs/2048.jpg)

内置的2048小游戏。


## 3.关于编译工程代码
1、本工程代码是基于vscode上的PlatformIO插件中的ESP32-Pico的Arduino平台开发。具体教程可以看[使用VScode开发ESP32，PlatformIO开发ESP32](https://blog.csdn.net/qlexcel/article/details/121527415)。

2、记得修改工程下`platformio.ini`文件中`upload_port`字段成对应自己COM口。


3、然后这里需要修改一个官方库文件才能正常使用：
PlatformIO和ArduinoIDE用户都需安装ESP32的Arduino固件支持包（百度有海量教程）。不管哪种开发方式都需要修改`SPI`库中的`MISO`默认引脚为`26`，例如arduinoIDE的包路径为`esp32\hardware\esp32\1.0.4\libraries\SPI\src\SPI.cpp`文件中，**修改以下代码中的MISO为26**：
```
    if(sck == -1 && miso == -1 && mosi == -1 && ss == -1) {
        _sck = (_spi_num == VSPI) ? SCK : 14;
        _miso = (_spi_num == VSPI) ? MISO : 12; // 需要改为26
        _mosi = (_spi_num == VSPI) ? MOSI : 13;
        _ss = (_spi_num == VSPI) ? SS : 15;
```

这是因为，硬件上连接屏幕和SD卡分别是用两个硬件SPI，其中HSPI的默认MISO引脚是12，而12在ESP32中是用于上电时设置flash电平的，上电之前上拉会导致芯片无法启动，因此我们将默认的引脚替换为26。



## 4. 参考项目
https://github.com/ClimbSnail/HoloCubic_AIO 

https://github.com/peng-zhihui/HoloCubic

